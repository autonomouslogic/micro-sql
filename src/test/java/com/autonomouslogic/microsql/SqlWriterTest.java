package com.autonomouslogic.microsql;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class SqlWriterTest {
	SqlWriter sqlWriter = new SqlWriter();
	List<Map<String, Object>> sampleRows;

	@Before
	public void before() throws Exception {
		sampleRows = new ArrayList<>(5);
		for (int i=0 ; i<5 ; i++) {
			Map row = new LinkedHashMap();
			sampleRows.add(row);
			// Shared rows.
			row.put("a", i);
			row.put("b", Integer.toString(i+10));
			// Unique row.
			row.put("c" + i, Integer.toString(i+20));
		}
	}

	@Test
	public void shouldCreateInsertStatements() throws Exception {
		String sql = sqlWriter.createInsert("tbl1", sampleRows.stream());
		String expected = "INSERT INTO `tbl1` (`a`, `b`, `c0`, `c1`, `c2`, `c4`) VALUES " +
			"(0, '10', '20', null, null, null, null), " +
			"(1, '11', null, '21', null, null, null), " +
			"(2, '12', null, null, '22', null, null), " +
			"(3, '13', null, null, null, '23', null), " +
			"(4, '14', null, null, null, null, '24') ";
		assertEquals(expected, sql);
	}
}
