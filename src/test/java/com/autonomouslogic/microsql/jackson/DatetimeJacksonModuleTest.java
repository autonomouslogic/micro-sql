package com.autonomouslogic.microsql.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

import static org.junit.Assert.assertEquals;

/**
 *
 */
public class DatetimeJacksonModuleTest {
	ObjectMapper objectMapper;
	JsonNodeFactory nodeFactory;

	@Before
	public void before() {
		objectMapper = new ObjectMapper();
		objectMapper.registerModule(new DatetimeJacksonModule());
		nodeFactory = objectMapper.getNodeFactory();
	}

	public static class DatetimeTestModel {
		public Instant instant;
	}

	public static Object[][] TEST_DATA = new Object[][] {
		new Object[] { Instant.parse("2018-05-04T04:07:15Z"), "2018-05-04 04:07:15.0" }
	};

	@Test
	public void shouldDecodeDatetimes() throws Exception {
		for (Object[] tuple : TEST_DATA) {
			Object decodedTimestamp = tuple[0];
			String encodedTimestamp = (String) tuple[1];
			ObjectNode json = nodeFactory.objectNode();
			json.set("instant", nodeFactory.textNode(encodedTimestamp));
			DatetimeTestModel decoded = objectMapper.readValue(json.toString(), DatetimeTestModel.class);
			assertEquals(decodedTimestamp, decoded.instant);
		}
	}
}
