package com.autonomouslogic.microsql;

import java.sql.Connection;

/**
 * Generic consumer-type interface for accepting and working with a SQL connection.
 */
public interface ConnectionConsumer {
	void accept(Connection connection) throws Exception;
}
