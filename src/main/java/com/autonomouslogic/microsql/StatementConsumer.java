package com.autonomouslogic.microsql;

import java.sql.Statement;

/**
 * Generic consumer-type interface for accepting and working with a SQL statement.
 */
public interface StatementConsumer {
	void accept(Statement connection) throws Exception;
}
