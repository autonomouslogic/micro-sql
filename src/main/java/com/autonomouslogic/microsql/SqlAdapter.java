package com.autonomouslogic.microsql;

import com.autonomouslogic.microsql.jackson.JacksonRowCodec;
import com.autonomouslogic.microsql.jackson.DatetimeJacksonModule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.*;

/**
 * Handler for access to SQL databases.
 */
@Deprecated
public class SqlAdapter {
	private static final Logger log = LoggerFactory.getLogger(SqlAdapter.class);

	private ObjectMapper objectMapper;
	private final GenericObjectPool<Connection> pool;

	public SqlAdapter(String url, String user, String password) {
		GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
		poolConfig.setMaxTotal(64);
		poolConfig.setMinIdle(1);
		poolConfig.setMaxIdle(5);
		poolConfig.setTestOnCreate(false);
		poolConfig.setTestOnBorrow(true);
		poolConfig.setTestOnReturn(false);
		poolConfig.setTestWhileIdle(false);
		pool = new ConnectionPool(new ConnectionFactory(url, user, password), poolConfig);
	}

	protected void init(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper.copy().registerModule(new DatetimeJacksonModule());
	}

	public <T> List<T> query(String sql, Class<T> clazz) throws SQLException {
		Objects.requireNonNull(sql, "sql");
		Connection connection = null;
		Statement statement = null;
		ResultSet result = null;
		try {
			connection = pool.borrowObject();
			statement = connection.createStatement();
			result = statement.executeQuery(sql);
			return decode(result, clazz);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			try {
				if (connection != null) pool.returnObject(connection);
			}
			catch (Exception e) {
				log.warn("Failed to return connection", e);
			}
			try {
				if (statement != null) statement.close();
			}
			catch (SQLException e) {
				log.warn("Failed to close statement or result", e);
			}
			try {
				if (statement != null) statement.close();
			}
			catch (SQLException e) {
				log.warn("Failed to close statement", e);
			}
			try {
				if (result != null) result.close();
			}
			catch (SQLException e) {
				log.warn("Failed to close result", e);
			}
		}
	}

	protected <T> List<T> decode(ResultSet resultSet, Class<T> clazz) throws SQLException, JsonProcessingException {
		// Get columns names.
		ResultSetMetaData meta = resultSet.getMetaData();
		int columnCount = meta.getColumnCount();
		List<String> columns = new ArrayList<>(columnCount);
		for (int i = 1; i <= columnCount; i++) {
			String label = meta.getColumnLabel(i);
			columns.add(label);
		}
		// Convert.
		List<T> list = new ArrayList<>();
		while (resultSet.next()) {
			// Construct JSON tree.
			ObjectNode obj = new ObjectNode(objectMapper.getNodeFactory());
			for (String column : columns) {
				Object val = resultSet.getObject(column);
				JsonNode node = JacksonRowCodec.toNode(val, objectMapper);
				obj.set(column, node);
			}
			list.add(objectMapper.treeToValue(obj, clazz));
		}
		return list;
	}

	public void execute(String sql) throws SQLException {
		Objects.requireNonNull(sql, "sql");
		Connection connection = null;
		Statement statement = null;
		try {
			connection = pool.borrowObject();
			statement = connection.createStatement();
			statement.execute(sql);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			try {
				if (statement != null) statement.close();
			}
			catch (SQLException e) {
				// Ignore.
			}
			try {
				if (connection != null) pool.returnObject(connection);
			}
			catch (Exception e) {
				// Ignore.
			}
		}
	}

	public <T> void insertIgnoreNew(String table, List<T> rows) throws SQLException, JsonProcessingException {
		// Decode to map. @todo optimise.
		List<Map> mapRows = new ArrayList<>(rows.size());
		for (T row : rows) {
			JsonNode json = objectMapper.valueToTree(row);
			Map map = objectMapper.treeToValue(json, Map.class);
			mapRows.add(map);
		}
		insertIgnore(table, mapRows);
	}

	@Deprecated // @todo remove and convert to using the above one exclusively.
	public void insertIgnore(String table, List<Map> rows) throws SQLException {
		Collection<String> columns = RowCodec.getColumns(rows);
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT IGNORE ").append(table).append(" (");
		StringJoiner columnJoiner = new StringJoiner(", ");
		for (String column : columns) {
			columnJoiner.add("`" + column + "`");
		}
		sql.append(columnJoiner).append(") VALUES ");
		StringJoiner rowsJoiner = new StringJoiner(", ");
		for (Map<String, Object> row : rows) {
			StringJoiner rowJoiner = new StringJoiner(", ", "(", ")");
			for (String column : columns) {
				Object val = row.get(column);
				if (val == null) {
					rowJoiner.add("null");
				}
				else if (val instanceof Boolean) {
					rowJoiner.add(val.toString());
				}
				else {
					rowJoiner.add('"' + val.toString() + '"');
				}
			}
			rowsJoiner.add(rowJoiner.toString());
		}
		sql.append(rowsJoiner.toString());
//		log.info(sql.toString());
		execute(sql.toString());
	}

	public <T> void insertUpdateNew(String table, List<T> rows) throws SQLException, JsonProcessingException {
		// Decode to map. @todo optimise.
		List<Map> mapRows = new ArrayList<>(rows.size());
		for (T row : rows) {
			JsonNode json = objectMapper.valueToTree(row);
			Map map = objectMapper.treeToValue(json, Map.class);
			mapRows.add(map);
		}
		insertUpdate(table, mapRows);
	}

	private void insertUpdate(String table, List<Map> rows) throws SQLException {
		for (Map<String, Object> row : rows) {
			insertUpdate(table, row);
		}
	}

	private void insertUpdate(String table, Map<String, Object> row) throws SQLException {
		Collection<String> columns = RowCodec.getColumns(Collections.singletonList(row));
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(table).append(" (");
		StringJoiner columnJoiner = new StringJoiner(", ");
		for (String column : columns) {
			columnJoiner.add("`" + column + "`");
		}
		sql.append(columnJoiner).append(") VALUES ");
		StringJoiner updatesJoiner = new StringJoiner(", ");
		StringJoiner rowJoiner = new StringJoiner(", ", "(", ")");
		for (String column : columns) {
			Object val = row.get(column);
			String stringVal;
			if (val == null) {
				stringVal = "null";
			}
			else if (val instanceof Boolean) {
				stringVal = val.toString();
			}
			else {
				stringVal = '"' + val.toString() + '"';
			}
			rowJoiner.add(stringVal);
			updatesJoiner.add("`" + column + "`=" + stringVal);
		}
		sql.append(rowJoiner.toString());
		sql.append(" ON DUPLICATE KEY UPDATE ").append(updatesJoiner.toString());
		execute(sql.toString());
	}

}
