package com.autonomouslogic.microsql;

import com.autonomouslogic.microsql.jackson.JacksonRowCodec;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.Statement;

/**
 * Primary interface for SQL access.
 * Use the {@link Builder} for construction.
 */
public class MicroSql {
	private static final Logger log = LoggerFactory.getLogger(MicroSql.class);

	/** The codec used for converting between POJOs and values going to and from SQL. */
	private RowCodec rowCodec;
	/** Writer for SQL statements. */
	private SqlWriter sqlWriter;
	/** Connection pool. */
	private ConnectionPool connectionPool;

	/**
	 * Constructor.
	 * Use the {@link Builder} for construction.
	 */
	protected MicroSql() {
	}

	/**
	 * Provides a connection, wrapping its consumption, and handles the subsequent cleanup.
	 * @param connectionConsumer
	 */
	public void useConnection(ConnectionConsumer connectionConsumer) throws Exception {
		Connection connection = null;
		try {
			connection = connectionPool.borrowObject();
			connectionConsumer.accept(connection);
		}
		finally {
			try {
				if (connection != null) connectionPool.returnObject(connection);
			}
			catch (Exception e) {
				log.warn("Failed to return connection to pool.", e);
				connectionPool.invalidateObject(connection);
			}
		}
	}

	/**
	 * Creates a statement and wraps its consumption and subsequent cleanup.
	 * @param statementConsumer
	 * @throws Exception
	 */
	public void useStatement(StatementConsumer statementConsumer) throws Exception {
		useConnection(connection -> {
			Statement statement = null;
			try {
				statement = connection.createStatement();
				statementConsumer.accept(statement);
			}
			finally {
				if (statement != null) statement.close();
			}
		});
	}

	/**
	 * Builder for {@link MicroSql} instances.
	 */
	public static class Builder {
		private RowCodec rowCodec;
		private String jdbc;
		private String user;
		private String password;
		private GenericObjectPoolConfig connectionPoolConfig;
		private ConnectionFactory connectionFactory;
		private ConnectionPool connectionPool;
		private SqlWriter sqlWriter;

		/**
		 * Builds the {@link MicroSql} instance, reverting to default values where appropriate.
		 * @return
		 */
		public MicroSql build() {
			MicroSql instance = new MicroSql();
			if (connectionPool == null) {
				if (connectionFactory == null) {
					if (jdbc == null) {
						throw new IllegalArgumentException("Either jdbc or connectionFactory must be set");
					}
					connectionFactory = new ConnectionFactory(jdbc, user, password);
				}
				if (connectionPoolConfig == null) {
					connectionPoolConfig = new GenericObjectPoolConfig();
					connectionPoolConfig.setMaxTotal(16);
					connectionPoolConfig.setMinIdle(1);
					connectionPoolConfig.setMaxIdle(1);
					connectionPoolConfig.setTestOnCreate(false);
					connectionPoolConfig.setTestOnBorrow(true);
					connectionPoolConfig.setTestOnReturn(false);
					connectionPoolConfig.setTestWhileIdle(false);
				}
				connectionPool = new ConnectionPool(connectionFactory, connectionPoolConfig);
			}
			instance.connectionPool = connectionPool;
			if (rowCodec == null) {
				rowCodec = new JacksonRowCodec();
			}
			instance.rowCodec = rowCodec;
			if (sqlWriter == null) {
				sqlWriter = new SqlWriter();
			}
			instance.sqlWriter = sqlWriter;
			return instance;
		}

		public Builder copy() {
			Builder copy = new Builder();
			copy.rowCodec = rowCodec;
			copy.jdbc = jdbc;
			copy.user = user;
			copy.password = password;
			copy.connectionPoolConfig = connectionPoolConfig;
			copy.connectionFactory = connectionFactory;
			copy.connectionPool = connectionPool;
			copy.sqlWriter = sqlWriter;
			return copy;
		}

		public RowCodec getRowCodec() {
			return rowCodec;
		}

		public Builder setRowCodec(RowCodec rowCodec) {
			this.rowCodec = rowCodec;
			return this;
		}

		public String getJdbc() {
			return jdbc;
		}

		public Builder setJdbc(String jdbc) {
			this.jdbc = jdbc;
			return this;
		}

		public String getUser() {
			return user;
		}

		public Builder setUser(String user) {
			this.user = user;
			return this;
		}

		public String getPassword() {
			return password;
		}

		public Builder setPassword(String password) {
			this.password = password;
			return this;
		}

		public GenericObjectPoolConfig getConnectionPoolConfig() {
			return connectionPoolConfig;
		}

		public Builder setConnectionPoolConfig(GenericObjectPoolConfig connectionPoolConfig) {
			this.connectionPoolConfig = connectionPoolConfig;
			return this;
		}

		public ConnectionFactory getConnectionFactory() {
			return connectionFactory;
		}

		public Builder setConnectionFactory(ConnectionFactory connectionFactory) {
			this.connectionFactory = connectionFactory;
			return this;
		}

		public ConnectionPool getConnectionPool() {
			return connectionPool;
		}

		public Builder setConnectionPool(ConnectionPool connectionPool) {
			this.connectionPool = connectionPool;
			return this;
		}

		public SqlWriter getSqlWriter() {
			return sqlWriter;
		}

		public Builder setSqlWriter(SqlWriter sqlWriter) {
			this.sqlWriter = sqlWriter;
			return this;
		}
	}
}
