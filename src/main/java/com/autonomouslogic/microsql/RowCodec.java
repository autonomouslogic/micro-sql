package com.autonomouslogic.microsql;

import java.util.*;
import java.util.stream.Stream;

/**
 * Encodes instances into {@link Map}s.
 */
public interface RowCodec {
	Map encodeRow(Object obj) throws Exception;

	default Stream<Map> encodeRows(Stream<Object> objs) throws Exception {
		return objs.map(obj -> {
			try {
				return encodeRow(obj);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		});
	}

	<T> T decodeRow(Map obj) throws Exception;

	/**
	 * Returns a collection of columns found in a list of maps.
	 * @param rows
	 * @return
	 */
	@SuppressWarnings("unchecked")
	static Collection<String> getColumns(List<Map> rows) {
		Set<String> columns = new LinkedHashSet<>();
		for (Map row : rows) {
			columns.addAll(row.keySet());
		}
		return columns;
	}

}
