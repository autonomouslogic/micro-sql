package com.autonomouslogic.microsql;

import com.google.common.collect.Iterators;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Helper class for writing SQL queries.
 */
public class SqlWriter {
	public enum OnDuplicateKey {
		DEFAULT,
		IGNORE,
		UPDATE
	}

	public String createInsert(String table, Stream<Map<String, Object>> rows) {
		return createInsert(table, rows, OnDuplicateKey.DEFAULT);
	}

	/**
	 * Creates an INSERT statement with the appropriate <code>ON DUPLICATE KEY</code> action.
	 * This will execute a terminating {@link Stream#forEach(Consumer)} on the <code>rows</code> supplied.
	 * @param table
	 * @param rows
	 * @param onDuplicateKey
	 * @return
	 */
	public String createInsert(String table, Stream<Map<String, Object>> rows, OnDuplicateKey onDuplicateKey) {
		if (onDuplicateKey == OnDuplicateKey.UPDATE) {
			throw new UnsupportedOperationException("OnDuplicateKey.UPDATE not yet implemented.");
		}
		if (onDuplicateKey == OnDuplicateKey.IGNORE) {
			throw new UnsupportedOperationException("OnDuplicateKey.IGNORE not yet implemented.");
		}
		Collection<String> columns = new LinkedHashSet<>();
		// Encode all the row values.
		StringJoiner rowsJoiner = new StringJoiner(", ");
		Iterator<Map<String, Object>> rowIterator = rows.iterator();
		while (rowIterator.hasNext()) {
			Map<String, Object> row = rowIterator.next();
			// The order of the rows matter. First encode known rows in order, then encode any new rows.
			Set<String> rowColumns = row.keySet();
			StringJoiner rowJoiner = new StringJoiner(", ", "(", ")");
			Iterator<String> allColumnsIterator = Iterators.concat(
				columns.iterator(),
				rowColumns.stream()
					.filter(c -> !columns.contains(c))
					.iterator()
			);
			while (allColumnsIterator.hasNext()) {
				String column = allColumnsIterator.next();
				Object val = row.get(column);
				if (val == null) {
					rowJoiner.add("null");
				}
				else if (val instanceof Boolean) {
					rowJoiner.add(val.toString());
				}
				else {
					rowJoiner.add('\'' + val.toString() + '\'');
				}
			}
			columns.addAll(rowColumns);
			rowsJoiner.add(rowJoiner.toString());
		}
		// Values have been encoded and we know the column names.
		StringJoiner columnJoiner = new StringJoiner(", ");
		for (String column : columns) {
			columnJoiner.add('`' + column + '`');
		}
		// OK, now we can build the final SQL statement.
		StringBuilder sql = new StringBuilder()
			.append("INSERT INTO ").append(table).append(" (")
			.append(columnJoiner)
			.append(") VALUES ")
			.append(rowsJoiner.toString());
		return sql.toString();
	}
}
