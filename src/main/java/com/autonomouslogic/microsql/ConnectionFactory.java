package com.autonomouslogic.microsql;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Creates new connections.
 */
public class ConnectionFactory extends BasePooledObjectFactory<Connection> {
	private Logger log = LoggerFactory.getLogger(ConnectionFactory.class);


	private final String jdbc;
	private final String user;
	private final String password;

	public ConnectionFactory(String jdbc, String user, String password) {
		this.jdbc = jdbc;
		this.user = user;
		this.password = password;
	}

	@Override
	public Connection create() throws Exception {
		return DriverManager.getConnection(jdbc, user, password);
	}

	@Override
	public PooledObject<Connection> wrap(Connection obj) {
		return new DefaultPooledObject<>(obj);
	}

	@Override
	public boolean validateObject(PooledObject<Connection> p) {
		Connection connection = p.getObject();
		try {
			if (connection.isClosed() || !connection.isValid(1)) {
				return false;
			}
			connection.createStatement().execute("select 0");
			return true;
		}
		catch (Throwable e1) {
			log.warn("Exception during connection validation: " + e1.getMessage());
			try {
				connection.close();
			}
			catch (SQLException e2) {
				// Empty.
			}
			return false;
		}
	}

	@Override
	public void destroyObject(PooledObject<Connection> p) throws Exception {
		p.getObject().close();
	}
}
