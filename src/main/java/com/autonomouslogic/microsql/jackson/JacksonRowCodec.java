package com.autonomouslogic.microsql.jackson;

import com.autonomouslogic.microsql.RowCodec;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Map;

/**
 * Uses Jackson to encode rows.
 */
public class JacksonRowCodec implements RowCodec {
	private final ObjectMapper objectMapper;

	/**
	 * Constructor.
	 */
	public JacksonRowCodec() {
		this(new ObjectMapper());
		objectMapper.registerModule(new DatetimeJacksonModule());
	}

	/**
	 * Constructor.
	 * @param objectMapper
	 */
	public JacksonRowCodec(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public Map encodeRow(Object obj) throws Exception {
		JsonNode json = objectMapper.valueToTree(obj);
		Map map = objectMapper.treeToValue(json, Map.class);
		return map;
	}

	@Override
	public <T> T decodeRow(Map obj) throws Exception {
		return null;
	}

	/**
	 * Converts an object to a JsonNode.
	 * @param val
	 * @param objectMapper
	 * @return
	 */
	public static JsonNode toNode(Object val, ObjectMapper objectMapper) {
		JsonNodeFactory nodeFactory = objectMapper.getNodeFactory();
		if (val instanceof String) {
			return nodeFactory.textNode((String) val);
		}
		else if (val instanceof BigDecimal) {
			return nodeFactory.numberNode((BigDecimal) val);
		}
		else if (val instanceof Integer) {
			return nodeFactory.numberNode((Integer) val);
		}
		else if (val instanceof Long) {
			return nodeFactory.numberNode((Long) val);
		}
		else if (val instanceof Boolean) {
			return nodeFactory.booleanNode((Boolean) val);
		}
		else if (val instanceof Timestamp) {
			return nodeFactory.textNode(val.toString());
		}
		else {
			throw new IllegalArgumentException(String.format("Unsupported node type: [%s] %s", val.getClass(), val.toString()));
		}
	}
}
