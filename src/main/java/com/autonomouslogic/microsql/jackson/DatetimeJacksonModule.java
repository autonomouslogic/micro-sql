package com.autonomouslogic.microsql.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

/**
 * Jackson module for serialization of various standard Java values to string, which are accepted by SQL databases.
 * The default implementation saves datetime values ({@link Instant}, {@link ZonedDateTime}, and {@link Date} as
 * ISO-like UTC timestamps without timezone information using the format <code>yyyy-MM-dd HH:mm:ss</code>.
 * Note that this effectively removes the time zone information stored in {@link ZonedDateTime}, which when read back
 * will be in UTC. This is because SQL <code>datetime</code> and <code>timestamp</code> data types do not carry time
 * zone information with them.
 */
public class DatetimeJacksonModule extends SimpleModule {
	public static final DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormatter.ISO_LOCAL_DATE;
	public static final DateTimeFormatter DEFAULT_TIME_FORMAT = DateTimeFormatter.ISO_LOCAL_TIME;
	public static final DateTimeFormatter DEFAULT_DATETIME_FORMAT = new DateTimeFormatterBuilder()
		.append(DEFAULT_DATE_FORMAT)
		.appendLiteral(' ')
		.append(DEFAULT_TIME_FORMAT)
		.toFormatter()
		.withZone(ZoneOffset.UTC);

	public DatetimeJacksonModule() {
		this(DEFAULT_DATE_FORMAT, DEFAULT_TIME_FORMAT, DEFAULT_DATETIME_FORMAT);
	}

	public DatetimeJacksonModule(DateTimeFormatter dateFormat, DateTimeFormatter timeFormat, DateTimeFormatter dateTimeFormat) {
		super(new Version(1, 0, 0, null, null, null));

		// Instant.
		addSerializer(Instant.class, new JsonSerializer<Instant>() {
			@Override
			public void serialize(Instant value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
				gen.writeString(dateTimeFormat.format(value));
			}
		});
		addDeserializer(Instant.class, new JsonDeserializer<Instant>() {
			@Override
			public Instant deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return Instant.from(dateTimeFormat.parse(p.getText()));
			}
		});

		// ZonedDateTime.
		addSerializer(ZonedDateTime.class, new JsonSerializer<ZonedDateTime>() {
			@Override
			public void serialize(ZonedDateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
				gen.writeString(dateTimeFormat.format(value));
			}
		});
		addDeserializer(ZonedDateTime.class, new JsonDeserializer<ZonedDateTime>() {
			@Override
			public ZonedDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return ZonedDateTime.from(dateTimeFormat.parse(p.getText()));
			}
		});

		// LocalDate.
		addSerializer(LocalDate.class, new JsonSerializer<LocalDate>() {
			@Override
			public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
				gen.writeString(dateFormat.format(value));
			}
		});
		addDeserializer(LocalDate.class, new JsonDeserializer<LocalDate>() {
			@Override
			public LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return LocalDate.from(dateFormat.parse(p.getText()));
			}
		});

		// LocalTime.
		addSerializer(LocalTime.class, new JsonSerializer<LocalTime>() {
			@Override
			public void serialize(LocalTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
				gen.writeString(timeFormat.format(value));
			}
		});
		addDeserializer(LocalTime.class, new JsonDeserializer<LocalTime>() {
			@Override
			public LocalTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return LocalTime.from(timeFormat.parse(p.getText()));
			}
		});

		// Date.
		addSerializer(Date.class, new JsonSerializer<Date>() {
			@Override
			public void serialize(Date value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
				gen.writeString(dateTimeFormat.format(value.toInstant()));
			}
		});
		addDeserializer(Date.class, new JsonDeserializer<Date>() {
			@Override
			public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
				return new Date(Instant.from(dateTimeFormat.parse(p.getText())).toEpochMilli());
			}
		});
	}
}
