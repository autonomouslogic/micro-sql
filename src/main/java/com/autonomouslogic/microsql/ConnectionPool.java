package com.autonomouslogic.microsql;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.sql.Connection;

/**
 * Pools connections.
 */
public class ConnectionPool extends GenericObjectPool<Connection> {
	public ConnectionPool(PooledObjectFactory<Connection> factory, GenericObjectPoolConfig config) {
		super(factory, config);
	}
}
