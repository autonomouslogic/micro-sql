# Micro SQL

**This project has been deprecated. You should use [JDBI](https://jdbi.org/) or [jOOQ](https://www.jooq.org/) instead. They do everything I was trying to do in this project.**

Micro SQL is a _very simple_ SQL wrapper for Java.
It was written to allow quick access to SQL databases with minimal initial setup.

Micro SQL handles all object encoding and decoding using Jackson Object Mapper,
and provides integrated helper methods for query generation.

It is designed for low intensity workloads.
If you have a big enterprise application, other libraries may suit you better.
If you have a medium-sized application where you just need to read a few hundred
rows here and there, Micro SQL could be your friend.
