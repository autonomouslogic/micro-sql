#!/bin/bash -e

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
hg="$(which hg)"

TAG="$($hg log --follow --template '{tags}\n' | egrep -v 'tip|^$' | head -n 1)"
REVTAG="$($hg parent --template '{tags}')"
ISODATE="$($hg log -l 1 --template '{date|isodatesec}')"
DATE=$(date -d "$ISODATE" -u "+%Y%m%d%H%M%S")
HASH="$($hg log -l 1 --template '{node|short}')"

if [ "$TAG" == "$REVTAG" ]; then
	VERSION="$TAG"
else
	VERSION="$TAG-$DATE+${HASH:0:7}"
fi

echo $VERSION
echo -n $VERSION > $DIR/../VERSION
